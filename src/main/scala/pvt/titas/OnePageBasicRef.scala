package pvt.titas

import com.typesafe.scalalogging.LazyLogging


/************************ Scala Types - A Few Words  ***********************************************
Any is the supertype of all types, also called the top type. It defines certain universal methods such as equals, hashCode, and toString.
Any has two direct subclasses: AnyVal and AnyRef.

AnyVal represents value types.
There are nine predefined value types and they are non-nullable: Double, Float, Long, Int, Short, Byte, Char, Unit, and Boolean.
Unit is a value type which carries no meaningful information.
There is exactly one instance of Unit which can be declared literally like so: ().
All functions must return something so sometimes Unit is a useful return type.

AnyRef represents reference types.
All non-value types are defined as reference types.
Every user-defined type in Scala is a subtype of AnyRef.
If Scala is used in the context of a Java runtime environment, AnyRef corresponds to java.lang.Object.
*******************************************************************************************************/

object OnePageBasicRef extends App with LazyLogging {
  println("Hello world in scala from Titas")
  //use of logger - import logger dependencies in build.sbt
  logger.info(args.mkString(", "))

  //String manipulation
  //case class is like pojo, without explicit requirement of accessor
  case class SampleClass(str: String, num: Int, bool: Boolean)

  val obj: SampleClass = SampleClass("text", 5, true)
  println(s"printing the objects value: ${obj.str} ${obj.num}")

  //Multi line or escape string with """
  val multiLineText: String =
    """|I will
       |run through
       |multiple lines
       |""".stripMargin

  println(multiLineText)

  //ternary op
  val fact = true;
  println(if (fact) ("Fact was true") else ("Fact was not true"))

  //for loop
  //inclusive range
  for (i <- 1 to 5)
    print(i + " ")
  print("\n")
  //excluding range
  for (i <- 1 until 5)
    print(i + " ")
  print("\n")

  //conditional print only even numbers
  for (i <- 1 to 5 if (i % 2 == 0))
    print(i + " ")
  print("\n")

  //yield
  val numbers = for {
    i <- 1 to 10
    if i % 2 == 0 || i % 5 == 0
  } yield i * 10
  print(numbers + " ")
  print("\n")
  //traverse 2d array
  val twoDimensionalArray = Array.ofDim[String](2, 2)
  twoDimensionalArray(0)(0) = "flour"
  twoDimensionalArray(0)(1) = "sugar"
  twoDimensionalArray(1)(0) = "egg"
  twoDimensionalArray(1)(1) = "syrup"
  for {x <- 0 until 2
       y <- 0 until 2
       } {
    println(s"item at position ${(x, y)} is ${twoDimensionalArray(x)(y)}")
  }

  //Range
  val from0To10By2 = 0 to 10 by 2
  for (i <- from0To10By2) print(i)

  print("\n")

  val charAtoZ = 'a' to 'z'
  for (i <- charAtoZ) print(i)
  print("\n")
  val charCapAtoZ = 'A' to 'Z' by 2
  for (i <- charCapAtoZ) print(i)
  print("\n")
  //mind the small 'z'
  val charCrazyAtoZ = 'A' to 'z'
  for (i <- charCrazyAtoZ) print(i)

  print("\n")

  //to collection
  val numRange = 1 to 5
  print(s"${numRange.toList} ${numRange.toList.mkString(" ")}\n")
  print(s"${numRange.toSet} ${numRange.toSet.mkString(" ")}\n")
  print(s"${numRange.toSeq} ${numRange.toSeq.mkString(" ")}\n")
  print(s"${numRange.toArray} ${numRange.toArray.mkString(" ")}\n")

  //for each
  numRange.toArray.foreach(print(_))
  print("\n")

  //while
  var index1 = 10
  while (index1 > 0) {
    print(index1 + " ")
    index1 -= 1
  }
  print("\n")
  do {
    index1 += 1
    print(index1 + " ")
  } while (index1 <= 5)
  print("\n")

  //pattern match
  //simple case type
  val pat = "abc"
  pat match {
    case "abc" => println("first 3 letter")
    case "def" => println("second 3 letters")
  }

  val matchReturn = pat match {
    case "abc" => 1
    case "def" => 2
  }
  println(matchReturn)

  //with multiple matching and default case
  val pat2 = "xyz"
  val matchReturn2 = pat2 match {
    case "xyz" | "abc" => 1
    case "def" => 2
    case _ => -1
  }
  println(matchReturn2)

  //conditional
  val pat3 = "xyz"
  val matchReturn3 = pat3 match {
    case ch if (ch.contains("xy") || ch.contains("yz")) => 1
    case "def" => 2
    case _ => -1
  }
  println(matchReturn3)

  //pattern matching by type
  val priceVal: Any = 2.50f
  val priceType = priceVal match {
    case price: Int => "Int"
    case price: Double => "Double"
    case price: Float => "Float"
    case price: String => "String"
    case price: Boolean => "Boolean"
    case price: Char => "Char"
    case price: Long => "Long"
  }
  println(priceType)

  //Tuples

  val tup2 = Tuple2(4, "Four")
  println(s"$tup2 -> ${tup2._1} | ${tup2._2}")
  //predefined till 22
  val tup5 = Tuple5(1, 2, 3, 4, 5)
  println(tup5)
  //shortcut to create tuples
  val tup5_2 = (1, 2, 3, 4)
  println(tup5_2)

  //tupels in pattern matching
  val tList = List((1, "one"), (2, "two"), (3, "three"), (4, "four"), (5, "five"), (6, "six"))

  val filteredList = tList.foreach { t => {
    t match {
      case d if d._1 % 2 == 1 => print(d)
      case _ => None
    }
  }
  }
  print("\n")

  //None, options, some
  val noneV: Option[String] = None;
  val v: Option[String] = Some("value")
  println(v.get) //not a good option in case of null/None
  //println(noneV.get) //exception
  println(noneV.getOrElse("No value present"))
  println(v.getOrElse("No value present"))
  //with pattern matching
  List(v, noneV).foreach(item => {
    item match {
      case Some(name) => println(name)
      case None => println("value not present")
    }
  })
  //filter with map function which automatically removes none - details later
  List(v,noneV).foreach(item=>{
    item.map(i=>println(s"Filtered by map $i"))
  })

}
