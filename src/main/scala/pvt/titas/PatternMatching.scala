package pvt.titas

object PatternMatching extends App {

  abstract class Notification

  case class Email(sender: String, subject: String, body: String) extends Notification

  case class SMS(caller: String, message: String) extends Notification

  case class VoiceRecording(contactName: String, link: String) extends Notification

  //simple pattern match with case classes
  def showNotification(notification: Notification): String = {
    notification match {
      case Email(sender, "urgent", _) => s"urgent email : $sender" //specific cases should be matched before more generic cases
      case Email(sender, subject, body) => s"email: $sender, $subject, $body"
      case SMS(caller, message) => s"sms : $caller"
      case VoiceRecording(contactName, _) => s"voice message : $contactName"
    }
  }

  val sms = SMS("1234", "abstract message")
  val email = Email("someone@email.com", "some normal message", "bla bla")
  val urgentEmail = Email("urgent@email.com", "urgent", "some urgent message")
  println(showNotification(sms))
  println(showNotification(email))
  println(showNotification(urgentEmail))

  //pattern guard
  def showImportantNotification(notification: Notification, importantContacts: Seq[String]): String = {
    notification match {
      case Email(sender, _, _) if importantContacts.contains(sender) => "email from somebody important"
      case SMS(caller, _) if importantContacts.contains(caller) => "sms from special person"
      case other => showNotification(other)
    }
  }

  val importantContacts = Seq("9999", "important@email.com")
  val importantEmail = Email("important@email.com", "imp", "imp")
  val importantSms = SMS("9999", "imp msg")
  val voiceRecording = VoiceRecording("contact", "abc")
  println(showImportantNotification(importantEmail, importantContacts))
  println(showImportantNotification(sms, importantContacts))
  println(showImportantNotification(importantSms, importantContacts))
  println(showImportantNotification(voiceRecording, importantContacts))

  //matching by only type

  abstract class Device

  case class Phone(model: String) extends Device {
    def screenOff = "turning off screen..."
  }

  case class Computer(model: String) extends Device {
    def screenSaverOn = "turning on screen saver...."
  }

  case class NoDevice() extends Device

  def goToIdle(device: Device): String = device match {
    case p: Phone => p.screenOff
    case c: Computer => c.screenSaverOn
    case other => "Nothing to do"
  }

  val phone = Phone("C687")
  val computer = Computer("MBP")
  val noDevice = NoDevice()
  println(goToIdle(phone))
  println(goToIdle(computer))
  println(goToIdle(noDevice))

  //Selaed classes
  /*****
   * Traits and classes can be marked sealed which means all subtypes must be declared in the same file.
   * This assures that all subtypes are known.
   */
  sealed abstract class Furniture

  case class Couch() extends Furniture

  case class Chair() extends Furniture

  def findPlaceToSit(piece: Furniture): String = piece match {
    case a: Couch => "Lie on the couch"
    case b: Chair => "Sit on the chair"
  }
}
